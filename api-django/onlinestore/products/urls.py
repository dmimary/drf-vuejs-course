from django.urls import path

from .views import (manufacturer_detail, manufacturers_list,
                    product_detail, product_list, 
                    ProductListView, ProductDetailView)   

urlpatterns = [
    path("products/", product_list, name="product-list"),
    path("products/<int:pk>/", product_detail, name="product-detail"),
    
    path("manufacturers/<int:pk>/", manufacturer_detail, name="manufacturer-detail"),
    path("manufacturers/", manufacturers_list, name="manufacturers")
]