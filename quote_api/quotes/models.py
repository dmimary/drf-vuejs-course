from django.db import models


class Quote(models.Model):

    quote_author = models.CharField(max_length=128)
    quote_body = models.TextField(blank=True, null=True)
    context = models.TextField(blank=True, null=True)
    source = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['created_at']
        verbose_name = 'Quote'
        verbose_name_plural = 'Quotes'

    def __str__(self):
        return self.quote_author if self.quote_author else 'Quote'
