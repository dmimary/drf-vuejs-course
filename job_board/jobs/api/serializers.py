from rest_framework import serializers

from job_board.jobs.models import JobOffer


class JobOfferSerializer(serializers.ModelSerializer):

    class Meta:
        model = JobOffer
        fields = '__all__'

    def validate(self, data):
        return super().validate(data)
        """ Check that description and title are different"""
        if data['job_title'] == data['job_description']:
            raise serializers.ValidationError('Title and Discription must be different from one another!')
        return data

    def validate_description(self, value):
        if len(value) < 60:
            raise serializers.ValidationError('The description must be at least 60 characters long1')
        return value
