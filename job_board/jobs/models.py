from django.db import models


class JobOffer(models.Model):

    company_name = models.CharField(max_length=120)
    company_email = models.EmailField()
    job_title = models.CharField(max_length=120)
    job_description = models.TextField()
    salary = models.FloatField()
    city = models.CharField(max_length=90)
    state = models.CharField(max_length=60)
    available = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{}: {}".format(self.company_name, self.job_title)
