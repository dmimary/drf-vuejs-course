from datetime import datetime
from django.utils.timesince import timesince
from rest_framework import serializers
from news.models import Article, Journalist


class ArticleSerializer(serializers.ModelSerializer):

    time_since_publication = serializers.SerializerMethodField()
    # author = serializers.StringRelatedField()  # represent strings -> first_name and last name
    # author = JournalistSerializer(read_only=True)

    class Meta:
        model = Article
        fields = '__all__'  # all fields to be serialized
        # fields = ['title', 'desciption', 'title']  # specific fields to be serialized
        # exclude = ['id']  # all fields appart from excluded to be serialized
    
    def get_time_since_publication(self, object):
        publication_date = object.publication_date
        time_delta = timesince(publication_date)
        return time_delta

    def validate(self, data):
        """ Check that description and title are different 
        https://www.django-rest-framework.org/api-guide/serializers/#object-level-validation
        """
        if data['title'] == data['description']:
            raise serializers.ValidationError('Title and Description must be different from one another!')
        return data

    def validate_body(self, value):
        """ Check that body is at least 60 characters long
        https://www.django-rest-framework.org/api-guide/serializers/#field-level-validation
        """
        if len(value) < 60:
            raise serializers.ValidationError('The article body must be at least 60 charachters long!')
        return value


class JournalistSerializer(serializers.ModelSerializer):
    # articles = ArticleSerializer(many=True, read_only=True)
    articles = serializers.HyperlinkedRelatedField(view_name='article-detail',
                                                   many=True, read_only = True)
                                                   
    class Meta:
        model = Journalist
        fields = '__all__'

# class ArticleSerializer(serializers.Serializer):
#     id = serializers.IntegerField(read_only=True)
#     author = serializers.CharField()
#     title = serializers.CharField()
#     description = serializers.CharField()
#     body = serializers.CharField()
#     location = serializers.CharField()
#     publication_date = serializers.DateField()
#     active = serializers.BooleanField()
#     created_at = serializers.DateTimeField(read_only=True)
#     updated_at = serializers.DateTimeField(read_only=True)

#     def create(self, validated_data):
#         print(validated_data)
#         return Article.objects.create(**validated_data)


#     def update(self, instance, validated_data):
#         instance.author = validated_data.get('author', instance.author)
#         instance.title = validated_data.get('title', instance.title)
#         instance.description = validated_data.get('description', 
#                                                   instance.description)
#         instance.body = validated_data.get('body', instance.body)
#         instance.location = validated_data.get('location', instance.location)
#         instance.publication_date = validated_data.get('publication_date', 
#                                                        instance.publication_date)
#         instance.active = validated_data.get('active', instance.active)

#         instance.save()
#         return instance

#     def validate(self, data):
#         """ Check that description and title are different """
#         if data['title'] == data['description']:
#             raise serializers.ValidationError('Title and Description must be different from one another!')
#         return data


#     def validate_title(self, value):
#         if len(value) < 60:
#             raise serializers.ValidationError('The title must be at least 60 carachters long!')
#         return value

