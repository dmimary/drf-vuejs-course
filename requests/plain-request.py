import requests

def main():
    response = requests.get('https://www.python.org/')
    print('Status code: ', response.status_code)
    print('Header: ', response.headers)
    print('Content-Type: ', response.headers['Content-Type'])
    print('Content: ', response.text)

if __name__ == "__main__":
    main()