from django.db import models
from django.contrib.auth.models import User


class Profile(models.Model):

    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
    )

    bio = models.CharField(
        max_length=255,
        blank=True,
        null=True,
    )

    city = models.CharField(
        max_length=64,
        blank=True,
        null=True,
    )

    avatar = models.ImageField(
        blank=True,
        null=True,
    )

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return f"{self.user.username}'s profile" if self.user.username else 'Profile'


class ProfileStatus(models.Model):

    user_profile = models.ForeignKey(
        Profile,
        on_delete=models.CASCADE,
    )

    status_content = models.CharField(
        max_length=255,
    )

    created_at = models.DateTimeField(
        auto_now_add=True,
    )

    updated_at = models.DateTimeField(
        auto_now=True,
    )

    class Meta:
        ordering = ['updated_at']
        verbose_name_plural = 'profile statuses'

    def __str__(self):
        return f'{str(self.user_profile)} status'
