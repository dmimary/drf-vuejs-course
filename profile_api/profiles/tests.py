import json

from django.urls import reverse
from django.contrib.auth.models import User

from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from .models import Profile, ProfileStatus
from .api.serializers import ProfileSerializer, ProfileStatusSerializer


class RegistrationTestCase(APITestCase):

    def test_registration(self):
        data = {
            'username': 'testuser',
            'email': 'testuser@email.com',
            'password1': 'qwerty2021',
            'password2': 'qwerty2021',  # password confirmation
        }

        response = self.client.post('/api/rest-auth/registration/', data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class ProfileViewSetTestCase(APITestCase):
    list_url = reverse('profile-list')

    def setUp(self):
        self.user = User.objects.create_user(
            username='mischief',
            password='qwerty2021',
        )

        self.token = Token.objects.create(user=self.user)
        self.api_authentication()

    def api_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.token.key}')

    def test_profile_list_authenticated(self):
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_profile_list_unauthenticated(self):
        self.client.force_authenticate(user=None)
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_profile_detail_retrieve(self):
        response = self.client.get(reverse('profile-detail', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['user'], 'mischief')

    def test_profile_update_by_owner(self):
        response = self.client.put(reverse('profile-detail', kwargs={'pk': 1}),
                                   {'city': 'Wonderland',
                                    'bio': 'Just living in a tale, u know'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content),
                         {'id': 1, 'user': 'mischief', 'bio': 'Just living in a tale, u know',
                          'city': 'Wonderland', 'avatar': None})

    def test_profile_update_by_random_user(self):
        random_user = User.objects.create_user(username='random', password='qwerty2020')
        self.client.force_authenticate(user=random_user)
        response = self.client.put(reverse('profile-detail', kwargs={'pk': 1}),
                                   {'bio': 'Ill-fated attempt to hack this poor fella'})
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ProfileStatusViewSetTestCase(APITestCase):
    url = reverse('status-list')

    def setUp(self):
        self.user = User.objects.create_user(
            username='mischief',
            password='qwerty2021',
        )
        self.status = ProfileStatus.objects.create(
            user_profile=self.user.profile,
            status_content='Being still alive'
        )
        self.token = Token.objects.create(user=self.user)
        self.api_authentication()

    def api_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION=f'Token {self.token.key}')

    def test_status_list_authenticated(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_status_list_unauthenticated(self):
        self.client.force_authenticate(user=None)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_status_create(self):
        data = {'status_content': 'Testing profile status create endpoint'}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['user_profile'], "mischief's profile")
        self.assertEqual(response.data['status_content'], 'Testing profile status create endpoint')

    def test_single_status_retrieve(self):
        serializer_data = ProfileStatusSerializer(instance=self.status).data
        response = self.client.get(reverse('status-detail', kwargs={'pk': 1}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response_data = json.loads(response.content)
        self.assertEqual(serializer_data, response_data)

    def test_status_update_by_owner(self):
        data = {'status_content': 'Testing profile status update endpoint'}
        response = self.client.put(reverse('status-detail', kwargs={'pk': 1}),
                                   data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['status_content'], 'Testing profile status update endpoint')

    def test_status_update_by_random_user(self):
        random_user = User.objects.create_user(username='random', password='qwerty2020')
        self.client.force_authenticate(user=random_user)
        data = {'status_content': 'Testing hacking the profile status update endpoint'}
        response = self.client.put(reverse('status-detail', kwargs={'pk': 1}),
                                   data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
