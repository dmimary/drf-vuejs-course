import requests


def client():
    token_header = 'Token 5a0d4bf7e98f81eb55b40712437a41899bec50ec'
    headers = {'Authorization': token_header}

    response = requests.get(
        'http://127.0.0.1:8899/api/profiles/',
        headers=headers,
    )

    print(f'Status Code: {response.status_code}')
    response_data = response.json()
    print(response_data)


if __name__ == '__main__':

    client()
