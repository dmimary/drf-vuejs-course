import requests


def client():
    data = {
        'username': 'dude',
        'email': 'dude@email.com',
        'password1': 'qwerty2021',
        'password2': 'qwerty2021',
    }

    url = 'http://127.0.0.1:8899/api/rest-auth/registration/'

    response = requests.post(
        url,
        data=data,
    )

    print(f'Status Code: {response.status_code}')
    response_data = response.json()
    print(response_data)


if __name__ == '__main__':
    client()
